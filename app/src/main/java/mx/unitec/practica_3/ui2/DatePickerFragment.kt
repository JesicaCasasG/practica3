package mx.unitec.practica_3.ui2

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import java.util.*

class DatePickerFragment: DialogFragment() {

    private var listener : DatePickerDialog.OnDateSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)


        return DatePickerDialog(activity, listener, year, month, day)

    }

  //  override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
    //    Toast.makeText(activity, "${year}:${month}:${day}", Toast.LENGTH_LONG).show()
    //}

    companion object {
        fun newInstance(listener: DatePickerDialog.OnDateSetListener) : DatePickerFragment {
            val fragment = DatePickerFragment()
            fragment.listener = listener
            return fragment
        }
    }
}