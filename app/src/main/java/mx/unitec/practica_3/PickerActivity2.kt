package mx.unitec.practica_3

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_picker2.*
import mx.unitec.practica_3.ui2.DatePickerFragment

class PickerActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picker2)
    }

    fun showDatePickerDialog(v: View){
       // val datePickerFragment = DatePickerFragment()
        val datePickerFragment = DatePickerFragment.newInstance(DatePickerDialog.OnDateSetListener
        { view, year, month, day ->

            pkrDate.setText("${year}:${month}:${day}")

        })

        datePickerFragment.show(supportFragmentManager, "datePicker")

    }
}